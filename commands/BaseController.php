<?php
/**
 * Created by PhpStorm.
 * User: Ruslan
 * Date: 26.03.2018
 * Time: 0:04
 */

namespace app\commands;

use yii\console\Controller;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use yii;

class BaseController extends Controller
{

    protected $connection;
    protected $channel;

    public function beforeAction($action)
    {
        $params = (object)Yii::$app->params['rabbit_params'];
        $this->connection = new AMQPStreamConnection($params->host, $params->port, $params->user, $params->password);
        $this->channel = $this->connection->channel();
        $this->channel->queue_declare('billing_messages_queue', false, false, false, false);
        return parent::beforeAction($action);
    }
}