<?php
/**
 * Клиент для тестирования  запросов взаимодействия
 * Created by PhpStorm.
 * User: Ruslan
 * Date: 25.03.2018
 * Time: 14:40
 */

namespace app\commands;

use PhpAmqpLib\Message\AMQPMessage;

class ClientController extends BaseController
{
    public function actionIndex()
    {
        $msg = new AMQPMessage(json_encode([
            'from' => 0,
            'to' => 2,
            'sum' => 20,
            'hash' => 'dfsdfweeret'
        ]));
        $this->channel->basic_publish($msg, '', 'billing_queue');
        echo " [x]Sent message!'\n";
    }
}
