<?php
/**
 * Воркер для обработки операций над балансом
 */

namespace app\commands;


use app\models\TransactionExecutor;
use app\models\User;
use app\models\validators\InputJsonSchemaValidator;
use Cocur\BackgroundProcess\BackgroundProcess;
use yii\console\ExitCode;

use yii;

class WorkerController extends BaseController
{
    public $json;

    public function options($actionID)
    {
        return ['json'];
    }

    public function beforeAction($action)
    {
        if ($this->action->id == "process-operation") {
            if (!InputJsonSchemaValidator::validate($this->json)) {
                return false;
            }
        }
        return parent::beforeAction($action);
    }

    public function actionProcessOperation()
    {
        $data = json_decode($this->json);
        $executor = new TransactionExecutor(Yii::$app->db);
        if ($executor->setSum($data->sum)
            ->setHash($data->hash)
            ->setFrom(User::findOne($data->from))
            ->setTo(User::findOne($data->to))
            ->executeOperation()) {
            $this->operationCallback($data->hash);
        }
        return ExitCode::OK;
    }

    /**
     * Метод генерации события
     * @param string $hash
     */
    public function operationCallback(string $hash)
    {
        echo "Здесь мы можем отправить в очередь событие о статусе выполнении операции";
    }

    public function actionIndex()
    {
        $callback = function ($msg) { //Отправляем выполнение операции в отдельный процесс
            $process = new BackgroundProcess("php yii worker/process-operation --json " . addslashes($msg->body));
            $process->run();
            echo " [x] Received ", $msg->body, "\n";
        };
        $this->channel->basic_consume('billing_queue', '', false, true, false, false, $callback);
        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }
    }
}
