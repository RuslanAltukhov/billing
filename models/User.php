<?php

namespace app\models;

use app\models\exceptions\NotEnoughtMoneyException;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property int $balance
 *
 * @property Transactions[] $transactions
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['balance'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'balance' => 'Balance',
        ];
    }

    /**
     * Зачисление средств на баланс пользователя
     * @param int $sum
     * @return bool
     */
    public function increaseBalance(int $sum): bool
    {
        $this->balance += $sum;
        return $this->save();
    }

    /**
     * Имеет ли пользователь на счету достаточно средств
     * @param int $sum
     * @return bool
     */
    public function hasEnoughtMoney(int $sum): bool
    {
        return $this->balance >= $sum;
    }

    /**
     * списание средств с баланса пользователя
     * @param int $sum
     * @return bool
     * @throws NotEnoughtMoneyException
     */
    public function decreaseBalance(int $sum): bool
    {
        if (!$this->hasEnoughtMoney($sum)) {
            throw  new NotEnoughtMoneyException();
        }
        $this->balance -= $sum;
        return $this->save();
    }


    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @inheritdoc
     * @return UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }
}
