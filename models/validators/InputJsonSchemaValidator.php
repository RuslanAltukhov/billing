<?php
/**
 * Created by PhpStorm.
 * User: Ruslan
 * Date: 26.03.2018
 * Time: 0:25
 */

namespace app\models\validators;

class InputJsonSchemaValidator
{
    /**
     * Правила валидации
     * @return array
     */
    public static function rules()
    {
        return [
            'from' => 'integer',
            'to' => 'integer',
            'sum' => 'integer',
            'hash' => 'string',
        ];
    }

    /**
     * Выполняет валидацию входных данных
     * @param string $json
     * @return bool
     */
    public static function validate(string $json): bool
    {
        $data = json_decode($json);
        foreach (self::rules() as $key => $value) {
            if (isset($data->{$key}) && gettype($data->{$key}) == $value) {
                continue;
            } else {
                return false;
                break;
            }
        }
        return true;
    }
}
