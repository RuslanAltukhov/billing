<?php
/**
 * Класс для работы с транзакциями
 * Created by PhpStorm.
 * User: Ruslan
 * Date: 25.03.2018
 * Time: 16:22
 */

namespace app\models;

use app\models\exceptions\NotEnoughtMoneyException;

class TransactionExecutor
{
    private $db;
    private $from;
    private $to;
    private $transaction;
    private static $class_name = "\app\models\Transaction";

    public function __construct(\yii\db\Connection $db)
    {
        $this->db = $db;
        $this->transaction = new self::$class_name;
    }

    /**
     * Установка класса для сохранения транзакции
     * @param string $class
     */
    public function setTransactionClass(string $class)
    {
        self::$class_name = $class;
    }

    public function setFrom(?User $from)
    {
        $this->from = $from;
        $from && $this->transaction->setFrom($from->id);
        return $this;
    }

    public function setHash(string $hash)
    {
        $this->transaction->setHash($hash);
        return $this;
    }

    public function setTo(?User $to)
    {
        $this->to = $to;
        $to && $this->transaction->setTo($to->id);
        return $this;
    }

    public function setSum(int $sum)
    {
        $this->transaction->setSum($sum);
        return $this;
    }

    /**
     * Установка типа транзакции в зависимости от переданных параметров
     */
    private function setType()
    {
        if ($this->from && $this->to) {
            $this->transaction->type = Transaction::TYPE_TRANSFER;
        } else {
            if ($this->from && !$this->to) {
                $this->transaction->type = Transaction::TYPE_OUTGOING;
            } else {
                if ($this->to && !$this->from) {
                    $this->transaction->type = Transaction::TYPE_INCOME;
                }
            }
        }
    }

    /**
     * Выполнение операции
     * @return bool
     * @throws \Exception
     */
    public function executeOperation(): bool
    {
        $transaction = $this->db->beginTransaction();
        $this->setType();
        try {
            $this->from && in_array($this->transaction->type, [
                Transaction::TYPE_TRANSFER,
                Transaction::TYPE_OUTGOING
            ]) && $this->from->decreaseBalance($this->transaction->sum);
            $this->to && $this->to->increaseBalance($this->transaction->sum);
            $this->transaction->save();
            $transaction->commit();
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        } catch (NotEnoughtMoneyException $e) { //Ловим исключение о недостатке средств на балансе
            $transaction->rollBack();
            return false;
        }
    }
}