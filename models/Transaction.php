<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transactions".
 *
 * @property int $id
 * @property string $date
 * @property int $sum
 * @property int $type Тип транзакции (1-пополнение баланса,2-списание,3-блокировка
 * @property int $status Статус транзакции (1-выполняется,2-выполнено
 * @property int $from
 * @property int $to
 */
class Transaction extends \yii\db\ActiveRecord
{
    const TYPE_INCOME = 1, TYPE_OUTGOING = 2, TYPE_TRANSFER = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['hash'], 'string'],
            [['type', 'from', 'to'], 'integer'],
            [['type'], 'required'],
            ['sum', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'integer'],
            ['type', 'validateType']
        ];
    }

    public function validateType()
    {
        if (!in_array($this->type, [self::TYPE_INCOME, self::TYPE_OUTGOING, self::TYPE_TRANSFER])) {
            $this->addError("type", "Не корректный тип транзакции");
        }
    }

    public function setFrom(int $from)
    {
        $this->from = $from;
        return $this;
    }

    public function setHash(string $hash)
    {
        $this->hash = $hash;
        return $this;
    }

    public function setTo(int $to)
    {
        $this->to = $to;
        return $this;
    }

    public function setType(int $type)
    {
        $this->type = $type;
        return $this;
    }

    public function setSum(int $sum)
    {
        $this->sum = $sum;
        return $this;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'sum' => 'Sum',
            'type' => 'Type',
            'from' => 'From',
            'to' => 'To',
        ];
    }

    /**
     * @inheritdoc
     * @return TransactionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TransactionsQuery(get_called_class());
    }
}
