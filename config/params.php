<?php

return [
    'adminEmail' => 'admin@example.com',
    'rabbit_params' => [
        'host' => 'localhost',
        'port' => 5672,
        'user' => 'guest',
        'password' => 'guest'
    ]
];
