<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transactions`.
 */
class m180325_115114_create_transactions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('transactions', [
            'id' => $this->primaryKey(),
            'date' => $this->timestamp()
                ->defaultExpression("CURRENT_TIMESTAMP"),
            'sum' => $this->integer(20)->unsigned()->notNull(),
            'type' => $this->smallInteger(1)
                ->notNull()
                ->comment("Тип транзакции (1-пополнение баланса,2-списание,3-перевод средств"),
            'from' => $this->integer(11)
                ->notNull()
                ->unsigned(),
            'to' => $this->integer(11)
                ->notNull()
                ->unsigned(),
            'hash' => $this->string(32)
                ->notNull()
                ->unique()
                ->comment("хэш транзакции. храним для идентификации транзакции во избежании повторов действия")
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('transactions');
    }
}
