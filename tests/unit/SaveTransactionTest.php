<?php

use app\models\Transaction;

class SaveTransactionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testSavingTransaction()
    {
        $model = new Transaction();
        $model->setType(Transaction::TYPE_INCOME);
        $model->setFrom(1);
        $model->setTo(1);
        $this->assertTrue($model->save());
    }
}