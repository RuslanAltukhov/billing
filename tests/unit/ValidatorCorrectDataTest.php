<?php

use app\models\validators\InputJsonSchemaValidator;

class ValidatorCorrectDataTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testValidatorCorrectData()
    {
        $this->assertTrue(InputJsonSchemaValidator::validate(json_encode([
            'sum' => 20,
            'from' => 2,
            'to' => 1,
            'hash' => 'abcdef'
        ])));
    }
}
