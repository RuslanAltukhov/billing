<?php

use app\models\Transaction;

class WrongTransactionSumTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testWronSum()
    {
        $model = new Transaction();
        $model->setType(Transaction::TYPE_INCOME);
        $model->setFrom(1);
        $model->setTo(2);
        $model->setSum(-20);
        $this->assertFalse($model->save());
    }
}
