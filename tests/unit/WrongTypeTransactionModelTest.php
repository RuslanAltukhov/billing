<?php

use app\models\Transaction;

class WrongTypeTransactionModelTest extends \Codeception\Test\Unit
{
    const WRONG_TYPE = 4;
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testWrongType()
    {
        $model = new Transaction();
        $model->setType(self::WRONG_TYPE);
        $this->assertFalse($model->save());
    }
}
