<?php

use app\models\validators\InputJsonSchemaValidator;

class ValidatorWrongDataTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testWrongValidationData()
    {
        $this->assertFalse(InputJsonSchemaValidator::validate(json_encode([
            'sum' => 'sdfsdf',
            'from' => 2,
            'to' => '2',
            'hash' => null
        ])));
    }
}
